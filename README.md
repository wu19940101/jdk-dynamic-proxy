# JDK 动态代理

#### 介绍
代理模式、作用、代理实现方式

#### 代理模式
当一个对象不能直接使用，可以在客户端和目标对象中直接创建一个中介，这个中介就是代理


#### 作用

1.  控制访问。在代理中，控制是否可以调用目标对象的方法
2.  功能增强。可以在完成目标对象的调用时，附加一些额外的的功能，这些额外的功能就是功能增强

#### 代理的实现方式

1.  静态代理：代理类是手工实现的Java文件，同时代理的目标对象是有规定的

    1）优点:容易理解，使用方便

    2）缺点：在目标比较多的时候，会产生大量的代理类；当接口改变时，影响的目标类和代理比较多，都需要跟着一起修改。

2.  动态代理：使用反射机制，在程序执行当中，创建代理对象。特点，不用创建类文件，代理的目标是活动的、可设置的

    1）不用创建代理类

    2）可以给不同的目标随时创建代理
